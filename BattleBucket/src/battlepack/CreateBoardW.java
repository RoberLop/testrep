package battlepack;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;

public class CreateBoardW extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateBoardW frame = new CreateBoardW();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateBoardW() {
		setTitle("Make Your Strategy");
		setBackground(Color.GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 550);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setBounds(10, 35, 480, 480);
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{60, 60, 60, 60, 60, 60, 60, 60, 0};
		gbl_panel.rowHeights = new int[]{60, 60, 60, 60, 60, 60, 60, 60, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lbl = new JLabel("");
		lbl.setBorder(new LineBorder(Color.WHITE));
		lbl.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_lbl = new GridBagConstraints();
		gbc_lbl.anchor = GridBagConstraints.NORTHWEST;
		gbc_lbl.gridx = 0;
		gbc_lbl.gridy = 0;
		panel.add(lbl, gbc_lbl);
		
		JLabel label = new JLabel("");
		label.setBorder(new LineBorder(Color.WHITE));
		label.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.NORTHWEST;
		gbc_label.gridx = 1;
		gbc_label.gridy = 0;
		panel.add(label, gbc_label);
		
		JLabel label_1 = new JLabel("");
		label_1.setBorder(new LineBorder(Color.WHITE));
		label_1.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 0;
		panel.add(label_1, gbc_label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setBorder(new LineBorder(Color.WHITE));
		label_2.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_2.gridx = 3;
		gbc_label_2.gridy = 0;
		panel.add(label_2, gbc_label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setBorder(new LineBorder(Color.WHITE));
		label_3.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_3.gridx = 4;
		gbc_label_3.gridy = 0;
		panel.add(label_3, gbc_label_3);
		
		JLabel label_4 = new JLabel("");
		label_4.setBorder(new LineBorder(Color.WHITE));
		label_4.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_4.gridx = 5;
		gbc_label_4.gridy = 0;
		panel.add(label_4, gbc_label_4);
		
		JLabel label_5 = new JLabel("");
		label_5.setBorder(new LineBorder(Color.WHITE));
		label_5.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_5.gridx = 6;
		gbc_label_5.gridy = 0;
		panel.add(label_5, gbc_label_5);
		
		JLabel label_55 = new JLabel("");
		label_55.setBorder(new LineBorder(Color.WHITE));
		label_55.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_55 = new GridBagConstraints();
		gbc_label_55.gridx = 7;
		gbc_label_55.gridy = 0;
		panel.add(label_55, gbc_label_55);
		
		JLabel label_6 = new JLabel("");
		label_6.setBorder(new LineBorder(Color.WHITE));
		label_6.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_6.gridx = 0;
		gbc_label_6.gridy = 1;
		panel.add(label_6, gbc_label_6);
		
		JLabel label_7 = new JLabel("");
		label_7.setBorder(new LineBorder(Color.WHITE));
		label_7.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_7.gridx = 1;
		gbc_label_7.gridy = 1;
		panel.add(label_7, gbc_label_7);
		
		JLabel label_8 = new JLabel("");
		label_8.setBorder(new LineBorder(Color.WHITE));
		label_8.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_8 = new GridBagConstraints();
		gbc_label_8.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_8.gridx = 2;
		gbc_label_8.gridy = 1;
		panel.add(label_8, gbc_label_8);
		
		JLabel label_9 = new JLabel("");
		label_9.setBorder(new LineBorder(Color.WHITE));
		label_9.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_9 = new GridBagConstraints();
		gbc_label_9.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_9.gridx = 3;
		gbc_label_9.gridy = 1;
		panel.add(label_9, gbc_label_9);
		
		JLabel label_10 = new JLabel("");
		label_10.setBorder(new LineBorder(Color.WHITE));
		label_10.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_10 = new GridBagConstraints();
		gbc_label_10.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_10.gridx = 4;
		gbc_label_10.gridy = 1;
		panel.add(label_10, gbc_label_10);
		
		JLabel label_11 = new JLabel("");
		label_11.setBorder(new LineBorder(Color.WHITE));
		label_11.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_11 = new GridBagConstraints();
		gbc_label_11.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_11.gridx = 5;
		gbc_label_11.gridy = 1;
		panel.add(label_11, gbc_label_11);
		
		JLabel label_12 = new JLabel("");
		label_12.setBorder(new LineBorder(Color.WHITE));
		label_12.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_12 = new GridBagConstraints();
		gbc_label_12.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_12.gridx = 6;
		gbc_label_12.gridy = 1;
		panel.add(label_12, gbc_label_12);
		
		JLabel label_56 = new JLabel("");
		label_56.setBorder(new LineBorder(Color.WHITE));
		label_56.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_56 = new GridBagConstraints();
		gbc_label_56.gridx = 7;
		gbc_label_56.gridy = 1;
		panel.add(label_56, gbc_label_56);
		
		JLabel label_13 = new JLabel("");
		label_13.setBorder(new LineBorder(Color.WHITE));
		label_13.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_13 = new GridBagConstraints();
		gbc_label_13.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_13.gridx = 0;
		gbc_label_13.gridy = 2;
		panel.add(label_13, gbc_label_13);
		
		JLabel label_14 = new JLabel("");
		label_14.setBorder(new LineBorder(Color.WHITE));
		label_14.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_14 = new GridBagConstraints();
		gbc_label_14.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_14.gridx = 1;
		gbc_label_14.gridy = 2;
		panel.add(label_14, gbc_label_14);
		
		JLabel label_15 = new JLabel("");
		label_15.setBorder(new LineBorder(Color.WHITE));
		label_15.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_15 = new GridBagConstraints();
		gbc_label_15.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_15.gridx = 2;
		gbc_label_15.gridy = 2;
		panel.add(label_15, gbc_label_15);
		
		JLabel label_16 = new JLabel("");
		label_16.setBorder(new LineBorder(Color.WHITE));
		label_16.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_16 = new GridBagConstraints();
		gbc_label_16.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_16.gridx = 3;
		gbc_label_16.gridy = 2;
		panel.add(label_16, gbc_label_16);
		
		JLabel label_17 = new JLabel("");
		label_17.setBorder(new LineBorder(Color.WHITE));
		label_17.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_17 = new GridBagConstraints();
		gbc_label_17.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_17.gridx = 4;
		gbc_label_17.gridy = 2;
		panel.add(label_17, gbc_label_17);
		
		JLabel label_18 = new JLabel("");
		label_18.setBorder(new LineBorder(Color.WHITE));
		label_18.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_18 = new GridBagConstraints();
		gbc_label_18.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_18.gridx = 5;
		gbc_label_18.gridy = 2;
		panel.add(label_18, gbc_label_18);
		
		JLabel label_19 = new JLabel("");
		label_19.setBorder(new LineBorder(Color.WHITE));
		label_19.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_19 = new GridBagConstraints();
		gbc_label_19.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_19.gridx = 6;
		gbc_label_19.gridy = 2;
		panel.add(label_19, gbc_label_19);
		
		JLabel label_57 = new JLabel("");
		label_57.setBorder(new LineBorder(Color.WHITE));
		label_57.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_57 = new GridBagConstraints();
		gbc_label_57.gridx = 7;
		gbc_label_57.gridy = 2;
		panel.add(label_57, gbc_label_57);
		
		JLabel label_20 = new JLabel("");
		label_20.setBorder(new LineBorder(Color.WHITE));
		label_20.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_20 = new GridBagConstraints();
		gbc_label_20.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_20.gridx = 0;
		gbc_label_20.gridy = 3;
		panel.add(label_20, gbc_label_20);
		
		JLabel label_21 = new JLabel("");
		label_21.setBorder(new LineBorder(Color.WHITE));
		label_21.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_21 = new GridBagConstraints();
		gbc_label_21.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_21.gridx = 1;
		gbc_label_21.gridy = 3;
		panel.add(label_21, gbc_label_21);
		
		JLabel label_22 = new JLabel("");
		label_22.setBorder(new LineBorder(Color.WHITE));
		label_22.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_22 = new GridBagConstraints();
		gbc_label_22.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_22.gridx = 2;
		gbc_label_22.gridy = 3;
		panel.add(label_22, gbc_label_22);
		
		JLabel label_23 = new JLabel("");
		label_23.setBorder(new LineBorder(Color.WHITE));
		label_23.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_23 = new GridBagConstraints();
		gbc_label_23.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_23.gridx = 3;
		gbc_label_23.gridy = 3;
		panel.add(label_23, gbc_label_23);
		
		JLabel label_24 = new JLabel("");
		label_24.setBorder(new LineBorder(Color.WHITE));
		label_24.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_24 = new GridBagConstraints();
		gbc_label_24.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_24.gridx = 4;
		gbc_label_24.gridy = 3;
		panel.add(label_24, gbc_label_24);
		
		JLabel label_25 = new JLabel("");
		label_25.setBorder(new LineBorder(Color.WHITE));
		label_25.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_25 = new GridBagConstraints();
		gbc_label_25.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_25.gridx = 5;
		gbc_label_25.gridy = 3;
		panel.add(label_25, gbc_label_25);
		
		JLabel label_26 = new JLabel("");
		label_26.setBorder(new LineBorder(Color.WHITE));
		label_26.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_26 = new GridBagConstraints();
		gbc_label_26.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_26.gridx = 6;
		gbc_label_26.gridy = 3;
		panel.add(label_26, gbc_label_26);
		
		JLabel label_58 = new JLabel("");
		label_58.setBorder(new LineBorder(Color.WHITE));
		label_58.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_58 = new GridBagConstraints();
		gbc_label_58.gridx = 7;
		gbc_label_58.gridy = 3;
		panel.add(label_58, gbc_label_58);
		
		JLabel label_27 = new JLabel("");
		label_27.setBorder(new LineBorder(Color.WHITE));
		label_27.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_27 = new GridBagConstraints();
		gbc_label_27.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_27.gridx = 0;
		gbc_label_27.gridy = 4;
		panel.add(label_27, gbc_label_27);
		
		JLabel label_28 = new JLabel("");
		label_28.setBorder(new LineBorder(Color.WHITE));
		label_28.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_28 = new GridBagConstraints();
		gbc_label_28.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_28.gridx = 1;
		gbc_label_28.gridy = 4;
		panel.add(label_28, gbc_label_28);
		
		JLabel label_29 = new JLabel("");
		label_29.setBorder(new LineBorder(Color.WHITE));
		label_29.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_29 = new GridBagConstraints();
		gbc_label_29.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_29.gridx = 2;
		gbc_label_29.gridy = 4;
		panel.add(label_29, gbc_label_29);
		
		JLabel label_30 = new JLabel("");
		label_30.setBorder(new LineBorder(Color.WHITE));
		label_30.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_30 = new GridBagConstraints();
		gbc_label_30.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_30.gridx = 3;
		gbc_label_30.gridy = 4;
		panel.add(label_30, gbc_label_30);
		
		JLabel label_31 = new JLabel("");
		label_31.setBorder(new LineBorder(Color.WHITE));
		label_31.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_31 = new GridBagConstraints();
		gbc_label_31.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_31.gridx = 4;
		gbc_label_31.gridy = 4;
		panel.add(label_31, gbc_label_31);
		
		JLabel label_32 = new JLabel("");
		label_32.setBorder(new LineBorder(Color.WHITE));
		label_32.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_32 = new GridBagConstraints();
		gbc_label_32.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_32.gridx = 5;
		gbc_label_32.gridy = 4;
		panel.add(label_32, gbc_label_32);
		
		JLabel label_33 = new JLabel("");
		label_33.setBorder(new LineBorder(Color.WHITE));
		label_33.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_33 = new GridBagConstraints();
		gbc_label_33.anchor = GridBagConstraints.NORTH;
		gbc_label_33.fill = GridBagConstraints.HORIZONTAL;
		gbc_label_33.gridx = 6;
		gbc_label_33.gridy = 4;
		panel.add(label_33, gbc_label_33);
		
		JLabel label_59 = new JLabel("");
		label_59.setBorder(new LineBorder(Color.WHITE));
		label_59.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_59 = new GridBagConstraints();
		gbc_label_59.gridx = 7;
		gbc_label_59.gridy = 4;
		panel.add(label_59, gbc_label_59);
		
		JLabel label_34 = new JLabel("");
		label_34.setBorder(new LineBorder(Color.WHITE));
		label_34.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_34 = new GridBagConstraints();
		gbc_label_34.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_34.gridx = 0;
		gbc_label_34.gridy = 5;
		panel.add(label_34, gbc_label_34);
		
		JLabel label_35 = new JLabel("");
		label_35.setBorder(new LineBorder(Color.WHITE));
		label_35.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_35 = new GridBagConstraints();
		gbc_label_35.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_35.gridx = 1;
		gbc_label_35.gridy = 5;
		panel.add(label_35, gbc_label_35);
		
		JLabel label_36 = new JLabel("");
		label_36.setBorder(new LineBorder(Color.WHITE));
		label_36.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_36 = new GridBagConstraints();
		gbc_label_36.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_36.gridx = 2;
		gbc_label_36.gridy = 5;
		panel.add(label_36, gbc_label_36);
		
		JLabel label_37 = new JLabel("");
		label_37.setBorder(new LineBorder(Color.WHITE));
		label_37.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_37 = new GridBagConstraints();
		gbc_label_37.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_37.gridx = 3;
		gbc_label_37.gridy = 5;
		panel.add(label_37, gbc_label_37);
		
		JLabel label_38 = new JLabel("");
		label_38.setBorder(new LineBorder(Color.WHITE));
		label_38.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_38 = new GridBagConstraints();
		gbc_label_38.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_38.gridx = 4;
		gbc_label_38.gridy = 5;
		panel.add(label_38, gbc_label_38);
		
		JLabel label_39 = new JLabel("");
		label_39.setBorder(new LineBorder(Color.WHITE));
		label_39.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_39 = new GridBagConstraints();
		gbc_label_39.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_39.gridx = 5;
		gbc_label_39.gridy = 5;
		panel.add(label_39, gbc_label_39);
		
		JLabel label_40 = new JLabel("");
		label_40.setBorder(new LineBorder(Color.WHITE));
		label_40.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_40 = new GridBagConstraints();
		gbc_label_40.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_40.gridx = 6;
		gbc_label_40.gridy = 5;
		panel.add(label_40, gbc_label_40);
		
		JLabel label_60 = new JLabel("");
		label_60.setBorder(new LineBorder(Color.WHITE));
		label_60.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_60 = new GridBagConstraints();
		gbc_label_60.gridx = 7;
		gbc_label_60.gridy = 5;
		panel.add(label_60, gbc_label_60);
		
		JLabel label_41 = new JLabel("");
		label_41.setBorder(new LineBorder(Color.WHITE));
		label_41.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_41 = new GridBagConstraints();
		gbc_label_41.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_41.gridx = 0;
		gbc_label_41.gridy = 6;
		panel.add(label_41, gbc_label_41);
		
		JLabel label_42 = new JLabel("");
		label_42.setBorder(new LineBorder(Color.WHITE));
		label_42.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_42 = new GridBagConstraints();
		gbc_label_42.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_42.gridx = 1;
		gbc_label_42.gridy = 6;
		panel.add(label_42, gbc_label_42);
		
		JLabel label_43 = new JLabel("");
		label_43.setBorder(new LineBorder(Color.WHITE));
		label_43.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_43 = new GridBagConstraints();
		gbc_label_43.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_43.gridx = 2;
		gbc_label_43.gridy = 6;
		panel.add(label_43, gbc_label_43);
		
		JLabel label_44 = new JLabel("");
		label_44.setBorder(new LineBorder(Color.WHITE));
		label_44.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_44 = new GridBagConstraints();
		gbc_label_44.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_44.gridx = 3;
		gbc_label_44.gridy = 6;
		panel.add(label_44, gbc_label_44);
		
		JLabel label_45 = new JLabel("");
		label_45.setBorder(new LineBorder(Color.WHITE));
		label_45.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_45 = new GridBagConstraints();
		gbc_label_45.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_45.gridx = 4;
		gbc_label_45.gridy = 6;
		panel.add(label_45, gbc_label_45);
		
		JLabel label_46 = new JLabel("");
		label_46.setBorder(new LineBorder(Color.WHITE));
		label_46.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_46 = new GridBagConstraints();
		gbc_label_46.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_46.gridx = 5;
		gbc_label_46.gridy = 6;
		panel.add(label_46, gbc_label_46);
		
		JLabel label_47 = new JLabel("");
		label_47.setBorder(new LineBorder(Color.WHITE));
		label_47.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_47 = new GridBagConstraints();
		gbc_label_47.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_47.gridx = 6;
		gbc_label_47.gridy = 6;
		panel.add(label_47, gbc_label_47);
		
		JLabel label_61 = new JLabel("");
		label_61.setBorder(new LineBorder(Color.WHITE));
		label_61.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_61 = new GridBagConstraints();
		gbc_label_61.gridx = 7;
		gbc_label_61.gridy = 6;
		panel.add(label_61, gbc_label_61);
		
		JLabel label_48 = new JLabel("");
		label_48.setBorder(new LineBorder(Color.WHITE));
		label_48.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_48 = new GridBagConstraints();
		gbc_label_48.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_48.gridx = 0;
		gbc_label_48.gridy = 7;
		panel.add(label_48, gbc_label_48);
		
		JLabel label_49 = new JLabel("");
		label_49.setBorder(new LineBorder(Color.WHITE));
		label_49.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_49 = new GridBagConstraints();
		gbc_label_49.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_49.gridx = 1;
		gbc_label_49.gridy = 7;
		panel.add(label_49, gbc_label_49);
		
		JLabel label_50 = new JLabel("");
		label_50.setBorder(new LineBorder(Color.WHITE));
		label_50.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_50 = new GridBagConstraints();
		gbc_label_50.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_50.gridx = 2;
		gbc_label_50.gridy = 7;
		panel.add(label_50, gbc_label_50);
		
		JLabel label_51 = new JLabel("");
		label_51.setBorder(new LineBorder(Color.WHITE));
		label_51.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_51 = new GridBagConstraints();
		gbc_label_51.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_51.gridx = 3;
		gbc_label_51.gridy = 7;
		panel.add(label_51, gbc_label_51);
		
		JLabel label_52 = new JLabel("");
		label_52.setBorder(new LineBorder(Color.WHITE));
		label_52.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_52 = new GridBagConstraints();
		gbc_label_52.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_52.gridx = 4;
		gbc_label_52.gridy = 7;
		panel.add(label_52, gbc_label_52);
		
		JLabel label_53 = new JLabel("");
		label_53.setBorder(new LineBorder(Color.WHITE));
		label_53.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_53 = new GridBagConstraints();
		gbc_label_53.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_53.gridx = 5;
		gbc_label_53.gridy = 7;
		panel.add(label_53, gbc_label_53);
		
		JLabel label_54 = new JLabel("");
		label_54.setBorder(new LineBorder(Color.WHITE));
		label_54.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_54 = new GridBagConstraints();
		gbc_label_54.anchor = GridBagConstraints.NORTHWEST;
		gbc_label_54.gridx = 6;
		gbc_label_54.gridy = 7;
		panel.add(label_54, gbc_label_54);
		
		JLabel label_62 = new JLabel("");
		label_62.setBorder(new LineBorder(Color.WHITE));
		label_62.setPreferredSize(new Dimension(60, 60));
		GridBagConstraints gbc_label_62 = new GridBagConstraints();
		gbc_label_62.gridx = 7;
		gbc_label_62.gridy = 7;
		panel.add(label_62, gbc_label_62);
		
		JLabel bglbl = new JLabel("");
		bglbl.setBounds(10, 35, 480, 480);
		contentPane.add(bglbl);
		
		Image image;
		Image newimg; 
		
		
		ImageIcon bg = new ImageIcon("./images/bg.png"); // load the image to a imageIcon
		image = bg.getImage(); // transform it 
		newimg = image.getScaledInstance(bglbl.getWidth(), bglbl.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		bg = new ImageIcon(newimg);  // transform it back
		
		bglbl.setIcon(bg);
		
		JLabel lblRemainingSpaceships = new JLabel("Remaining Spaceships");
		lblRemainingSpaceships.setFont(new Font("Impact", Font.BOLD, 21));
		lblRemainingSpaceships.setForeground(Color.LIGHT_GRAY);
		lblRemainingSpaceships.setBounds(508, 12, 222, 41);
		contentPane.add(lblRemainingSpaceships);
		
		JPanel shipsPanel = new JPanel();
		shipsPanel.setBackground(Color.LIGHT_GRAY);
		shipsPanel.setBounds(500, 60, 230, 400);
		contentPane.add(shipsPanel);
		shipsPanel.setLayout(null);
		
		
		
		JLabel lblImgx1 = new JLabel("");
		lblImgx1.setBackground(Color.RED);
		lblImgx1.setBounds(12, 76, 45, 60);
		shipsPanel.add(lblImgx1);
		
		ImageIcon ssx1 = new ImageIcon("./images/ssx1.png"); // load the image to a imageIcon
		image = ssx1.getImage(); // transform it 
		newimg = image.getScaledInstance(lblImgx1.getWidth(), lblImgx1.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		ssx1 = new ImageIcon(newimg);  // transform it back
		
		lblImgx1.setIcon(ssx1);
		
		JLabel lblImgx2 = new JLabel("");
		lblImgx2.setBackground(Color.RED);
		lblImgx2.setBounds(67, 46, 45, 90);
		shipsPanel.add(lblImgx2);
		
		
		ImageIcon ssx2 = new ImageIcon("./images/ssx2.png"); // load the image to a imageIcon
		image = ssx2.getImage(); // transform it 
		newimg = image.getScaledInstance(lblImgx2.getWidth(), lblImgx2.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		ssx1 = new ImageIcon(newimg);  // transform it back
		
		lblImgx2.setIcon(ssx2);
		
		JLabel lblImgx4 = new JLabel("");
		lblImgx4.setBackground(Color.RED);
		lblImgx4.setBounds(117, 46, 90, 90);
		shipsPanel.add(lblImgx4);
		
		
		ImageIcon ssx4 = new ImageIcon("./images/ssx4.png"); // load the image to a imageIcon
		image = ssx4.getImage(); // transform it 
		newimg = image.getScaledInstance(lblImgx4.getWidth(), lblImgx4.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		ssx4 = new ImageIcon(newimg);  // transform it back
		
		lblImgx4.setIcon(ssx4);
		
		JLabel lblX = new JLabel("x1");
		lblX.setBounds(29, 136, 28, 15);
		shipsPanel.add(lblX);
		
		JLabel lblX_1 = new JLabel("x2");
		lblX_1.setBounds(82, 136, 28, 15);
		shipsPanel.add(lblX_1);
		
		JLabel lblX_2 = new JLabel("x4");
		lblX_2.setBounds(154, 136, 28, 15);
		shipsPanel.add(lblX_2);
		
		JLabel lblImgx3 = new JLabel("");
		lblImgx3.setBackground(Color.RED);
		lblImgx3.setBounds(20, 225, 50, 110);
		shipsPanel.add(lblImgx3);
		
		
		ImageIcon ssx3 = new ImageIcon("./images/ssx3.png"); // load the image to a imageIcon
		image = ssx3.getImage(); // transform it 
		newimg = image.getScaledInstance(lblImgx3.getWidth(), lblImgx3.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		ssx3 = new ImageIcon(newimg);  // transform it back
		
		lblImgx3.setIcon(ssx3);
		
		
		JLabel lblImgx5 = new JLabel("");
		lblImgx5.setBackground(Color.RED);
		lblImgx5.setBounds(75, 205, 50, 130);
		shipsPanel.add(lblImgx5);
		
		
		ImageIcon ssx5 = new ImageIcon("./images/ssx5.png"); // load the image to a imageIcon
		image = ssx5.getImage(); // transform it 
		newimg = image.getScaledInstance(lblImgx5.getWidth(), lblImgx5.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		ssx5 = new ImageIcon(newimg);  // transform it back
		
		lblImgx5.setIcon(ssx5);
		
		JLabel lblX_3 = new JLabel("x3");
		lblX_3.setBounds(37, 332, 28, 15);
		shipsPanel.add(lblX_3);
		
		JLabel lblX_4 = new JLabel("x4");
		lblX_4.setBounds(95, 332, 28, 15);
		shipsPanel.add(lblX_4);
		
		JLabel bhlbl = new JLabel("");
		bhlbl.setBackground(Color.RED);
		bhlbl.setBounds(140, 230, 75, 75);
		shipsPanel.add(bhlbl);
		
		ImageIcon bhimg = new ImageIcon("./images/blackhole.png"); // load the image to a imageIcon
		image = bhimg.getImage(); // transform it 
		newimg = image.getScaledInstance(bhlbl.getWidth(), bhlbl.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		bhimg = new ImageIcon(newimg);  // transform it back
		
		bhlbl.setIcon(bhimg);
		
		JLabel lblBlackhole = new JLabel("Blackhole");
		lblBlackhole.setBounds(148, 332, 70, 15);
		shipsPanel.add(lblBlackhole);
		
		ImageIcon bhimg2 = new ImageIcon("./images/blackhole.png"); // load the image to a imageIcon
		image = bhimg2.getImage();
		bhimg2 = new ImageIcon(newimg);
		
		
		
		JButton btnStart = new JButton("START!");
		btnStart.setFont(new Font("Impact", Font.BOLD, 20));
		btnStart.setForeground(Color.WHITE);
		btnStart.setBackground(Color.RED);
		btnStart.setBounds(555, 475, 117, 25);
		contentPane.add(btnStart);
		
		JLabel lblPlaceYourShips = new JLabel("Place your Ships");
		lblPlaceYourShips.setForeground(Color.LIGHT_GRAY);
		lblPlaceYourShips.setFont(new Font("Impact", Font.BOLD, 18));
		lblPlaceYourShips.setBounds(177, 0, 153, 32);
		contentPane.add(lblPlaceYourShips);
		
		
		
	}
}
